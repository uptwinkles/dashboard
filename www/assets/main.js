/**
 *
 * @licstart  The following is the entire license notice for the
 *  JavaScript code in this page.
 *
 * Copyright (C) 2017  Uptwinkles Residents
 *
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 * for the JavaScript code in this page.
 *
 */

 function getToday() {
   var today = new Date();
   var year = today.getFullYear();
   var month = today.getMonth() + 1;
   var day = today.getDate();
   if (month < 10) month = '0' + month;
   if (day < 10) day = '0' + day;
   return year + '-' + month + '-' + day;
 }

function getChores() {
  var ul = document.querySelector(".chore_assignments");

  var r = new XMLHttpRequest();

  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;

    var chores = JSON.parse(r.responseText).assignments;

    ul.innerHTML = '';

    for (var i=0; i<chores.length; i++) {
      var chore = chores[i];
      var choreName = chore.chore.short_name;
      var assignee = chore.person.first_name;
      var chorePic = chore.chore.image;
      var assigneePic = chore.person.avatar_url;
      if (!assigneePic) {
        assigneePic = "https://via.placeholder.com/500/3A0031/FFFFFF?text=" + assignee[0];
      }
      var li = document.createElement("li");
      li.innerHTML = "" +
      '<div class="chore_item">' +
        '<p class="chore_title">' + choreName + '</p>' +
        // FIXME: Due to a bug in the API, we have to put this URL fragment below. Shouldn't be necessary!
        '<img src="https://api.uptwinkles.co' + chorePic + '" class="chore_pic">' +
        '<img src="' + assigneePic + '" class="profile_pic">' +
      '</div>';
      ul.appendChild(li);
    }
  };

  var today = getToday();
  r.open('GET', 'https://api.uptwinkles.co/chore-assignments/'+today+'?format=json', true);
  r.send();
}

function getDinner() {
  var h2 = document.querySelector(".dinner-assignee");
  var p = document.querySelector(".dinner .suggestion");
  var r = new XMLHttpRequest();

  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;

    var dinner = JSON.parse(r.responseText);
    h2.innerHTML = dinner.person.first_name;
    var suggestion = JSON.parse(r.responseText);
    p.innerHTML = "What about " + dinner.meal.name + "?";
  };

  var today = getToday();
  r.open('GET', 'https://api.uptwinkles.co/dinner-assignments/'+today+'?format=json', true);
  r.send();
}

function getDishes() {
  var h2 = document.querySelector(".dishes-assignee");
  var r = new XMLHttpRequest();

  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;
    var dishes = JSON.parse(r.responseText);
    h2.innerHTML = dishes.person.first_name;
  };

  var today = getToday();
  r.open('GET', 'https://api.uptwinkles.co/dishes-assignments/'+today+'?format=json', true);
  r.send();
}

function getCleanup() {
  var h2 = document.querySelector(".cleanup-assignee");
  var r = new XMLHttpRequest();

  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;
    var cleanup = JSON.parse(r.responseText);
    h2.innerHTML = cleanup.person.first_name;
  };

  var today = getToday();
  r.open('GET', 'https://api.uptwinkles.co/cleanup-assignments/'+today+'?format=json', true);
  r.send();
}

function getBackup() {
  var h1 = document.querySelector(".backup-assignee");
  var r = new XMLHttpRequest();

  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) return;
    var backup = JSON.parse(r.responseText);
    h1.innerHTML = backup.person.first_name;
  };

  var today = getToday();
  r.open('GET', 'https://api.uptwinkles.co/backup-assignments/'+today+'?format=json', true);
  r.send();
}

function markCalendar() {
  var d = new Date();
  var n = (d.getDay() + 6) % 7;
  var days = document.querySelectorAll(".calendar .day");
  for (var i=0; i<days.length; i++) {
    if (i<n) {
      days[i].className = "day passed";
    }
    if (i==n) {
      days[i].className = "day today";
    }
    if (i>n)  {
      days[i].className = "day future";
    }
  }
}

function clockTick() {
  var d = new Date();
  var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  var hour = d.getHours();
  var period;  // AM/PM
  if (hour >= 12) {
    period = "pm";
  } else {
    period = "am";
  }
  hour = hour % 12;
  if (hour == 0)
    hour = 12;

  function addZero(i) {
    if (i < 10)
        i = "0" + i;
    return i;
  }

  document.getElementById("dayofweek").innerHTML = days[d.getDay()];
  document.getElementById("month").innerHTML = months[d.getMonth()];
  document.getElementById("day").innerHTML = d.getDate();
  document.getElementById("year").innerHTML = d.getFullYear();
  document.getElementById("hour").innerHTML = hour;
  document.getElementById("minute").innerHTML = addZero(d.getMinutes());
  document.getElementById("period").innerHTML = period;
}

function hourly() {
  // Run hourly: API stuff
  getChores();
  getDinner();
  getDishes();
  getCleanup();
  getBackup();
}

function secondly() {
  // Run each second
  clockTick();
  markCalendar();
}

hourly();
secondly();
setInterval(hourly, 60000);
setInterval(secondly, 1000);
