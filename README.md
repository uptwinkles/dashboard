# Uptwinkles Dashboard

Used to help us keep track of chores and other tasks/events. It reads from [Uptwinkles/api](https://github.com/Uptwinkles/api) and we use it to display information on a screen in our kitchen.

![Dashboard photo](dashboard-photo.jpg)

## License

Uptwinkles Dashboard is licensed under GNU GPL version 3.0. For the full license see the LICENSE file.
